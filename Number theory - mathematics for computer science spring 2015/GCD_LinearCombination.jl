import Base

function GCD(x, y)
    z = Base.gcd(x, y)
    return z
end

function linearcomb(x, y, z, s, t)
    if Base.rem(z, x) == 0 & Base.rem(z, y) == 0
        @assert Base.rem((s*y+t*x), z) == 0
    end
end

